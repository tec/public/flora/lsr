#!/usr/bin/env python3

import serial
import time
import sys
import base64
import binascii
import socket


# config
host_id        = 7
use_socket     = True               # set to True to interact with a flocklab node; when set to False, the script will connect to the specified serial port instead
serialport     = '/dev/ttyUSB1'     # serial port to use when 'use_socket' is False
baudrate       = 1000000            # baudrate for the serial port
socketport     = 50100 + host_id    # port number is 50100 + observer ID
hostname       = "flocklab.ethz.ch"

read_delay     = 0.05
b64_identifier = "B64="
max_pkt_len    = 256
pkt_hdr_len    = 10


def compose_packet(pkt_type, payload):
    # compose header
    data        = bytearray(pkt_hdr_len)
    data[4]     = pkt_type
    data[5]     = len(payload)
    # append payload
    data       += bytearray(payload, 'utf-8')
    # calculate crc
    data[0:4]   = binascii.crc32(data[4:]).to_bytes(4, 'little')
    data_enc    = base64.b64encode(data)
    data_enc   += b'\n'
    return data_enc


def parse_data(data):
    for line in data.split('\n'):
        if b64_identifier in line:
            content = line.split(b64_identifier)[1]
            try:
                pkt      = base64.b64decode(content)
                pkt_type = pkt[4]
                pkt_len  = pkt[5]
                pkt_crc  = int.from_bytes(pkt[0:4], 'little')
                crc      = binascii.crc32(pkt[4:])
                print("packet of type %u received (%u bytes)" % (pkt_type, pkt_len))
                # check length and crc
                if pkt_len <= 0 or pkt_len > (max_pkt_len - pkt_hdr_len):
                    print("invalid payload length")
                elif crc != pkt_crc:
                    print("invalid CRC")
                else:
                    print("packet handling")  # TODO handle data
                    return True
            except:
                print("failed to decode %s" % content)
        else:
            print(line.strip())
    return False


def read_from_serial():
    with serial.Serial(serialport, baudrate) as ser:
        packet  = []
        while True:
            if ser.inWaiting() > 0:
                time.sleep(read_delay)
                data = ser.read(ser.inWaiting()).decode('UTF-8').strip()
                if parse_data(data):
                    pkt = compose_packet(2, 'hello world!')   # TODO set meaningful content
                    ser.write(pkt)
            time.sleep(0.001)


def read_from_socket():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((hostname, socketport))
    try:
        while True:
            data = sock.recv(1024)
            if data:
                if parse_data(data.decode()):
                    pkt = compose_packet(2, 'hello world!')   # TODO set meaningful content
                    sock.sendall(pkt)
    except:
        sock.close()
        raise


try:
    if use_socket:
        read_from_socket()
    else:
        read_from_serial()
except serial.serialutil.SerialException:
    print("failed to open serial port %s" % (serialport,))
except ConnectionRefusedError:
    print("cannot connect to host %s on port %u" % (hostname, socketport))
except KeyboardInterrupt:
    print("\b\baborted")
