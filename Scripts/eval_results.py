#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: romantrueb
@brief:  Anaylse results from running LSR protocol on flocklab
"""

import sys
import os
import numpy as np
import pandas as pd
import json
from collections import OrderedDict, Counter
import pickle
from copy import copy
from rocketlogger.data import RocketLoggerData

# construct html with python
import dominate
from dominate.tags import *
from dominate.util import raw

from flocklab import Flocklab
from flocklab import *

fl = Flocklab()

################################################################################

outputDir = './output'
plotTitle = False

GPIO_FLOOD = 'INT2'
GPIO_TX = 'LED2'
GPIO_RX = 'LED3'

################################################################################

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import matplotlib.transforms as mtransforms
plt.close('all')

################################################################################
# Helper Functions
################################################################################

def getJson(text):
    '''Find an convert json in a single line from serial output. Returns None if no valid json could be found.
    '''
    ret = None
    # find index
    idx = 0
    if not '{' in text:
        return ret
    for i in range(len(text)):
        if text[i] == '{':
            idx = i
            break

    try:
        ret =  json.loads(text[idx:], strict=False)
    except json.JSONDecodeError:
        print('WARNING: json could not be parsed: {}'.format(text[idx:]))
    return ret

def getPinStateAtTime(ts, pinDf):
    '''Determine pin state at time ts using pinDf
    Assumption: pinDf contains pin state changes of node and pin of interest, at least up to time ts (additional state changes are fine)
    '''
    df = pinDf[(pinDf.timestamp <= ts)]
    return df.iloc[-1]['value']

################################################################################

def extractSerialData(testNo, testDir):
    serialPath = os.path.join(testDir, "{}/serial.csv".format(testNo))

    # download test results if directory does not exist
    if not os.path.isfile(serialPath):
        fl.getResults(testNo)

    df = fl.serial2Df(serialPath, error='ignore')
    df.sort_values(by=['timestamp', 'observer_id'], inplace=True, ignore_index=True)

    # convert output with valid json to dict and remove other rows
    keepMask = []
    resList = []
    for idx, row in df.iterrows():
        jsonDict = getJson(row['output'])
        keepMask.append(1 if jsonDict else 0)
        if jsonDict:
            resList.append(jsonDict)
    dfd = df[np.asarray(keepMask).astype(bool)].copy()
    dfd['data'] = resList

    # determine hostNodeId
    maskPreTask = np.asarray([e['type'] == 'PreTask' for e in dfd['data'].to_numpy()])
    hostNodesTmp = dfd[maskPreTask]['observer_id']
    s = set(hostNodesTmp)
    if len(s) > 1:
        raise Exception('Multiple hostIds detected! {}'.format(list(s)))
    hostNodeId = list(s)[0]

    # determine nodeIds
    allNodeIds = sorted(dfd.observer_id.unique())
    nodeIds = copy(allNodeIds)
    nodeIds.remove(hostNodeId)

    print('nodeIds: {}'.format(nodeIds))
    print('hostNodeId: {}'.format(hostNodeId))

    # determine round starts
    if len(set(dfd[maskPreTask]['observer_id'])) > 1:
        raise Exception('More than one node is logging PreTask events!')
    tsRoundsAll = dfd[maskPreTask]['timestamp'].to_numpy()
    tsRounds = tsRoundsAll
    print('Found {} round starts, using {} rounds'.format(len(tsRoundsAll), len(tsRounds)-1))

    return dfd, tsRounds, hostNodeId, nodeIds


def extractReliability(dfd, tsRounds, hostNodeId, nodeIds, debug=False):
    ''' Extract number of sent and received packets (for calculating reliability metric) from flocklab test results
    Args:
      dfd:        Dataframe containing the flocklab serial log plus the decoded json objects in the `data` column
      tsRounds:   List of round start timestamps to consider (note last element is used as end of round only, corresponding round is ignored)
      nodeIds:    List of all node IDs (except host)
      hostNodeId: ID of host node
    '''
    print('=== extractReliability ===')

    # prepare data structures for collecting stats
    txPkts = OrderedDict([(nodeId, OrderedDict()) for nodeId in nodeIds])
    rxPkts = OrderedDict([(nodeId, OrderedDict()) for nodeId in nodeIds])

    # reliability: iterate over round
    for roundIdx in range(len(tsRounds)-1):
        dfdTmp = dfd[(dfd.timestamp >= tsRounds[roundIdx]) & (dfd.timestamp < tsRounds[roundIdx+1])]
        for nodeId in nodeIds:
            txPkts[nodeId][roundIdx] = 0
            rxPkts[nodeId][roundIdx] = 0
        txPktsRound = [] # List of nodeIds (one element for each transmission)
        rxPktsRound = [] # List of nodeIds (one element for each transmission)
        for idx, row in dfdTmp.iterrows():
            if row['data']['type'] == 'DataPktTx' and row['data']['sent'] == 1:
                txPktsRound.append(row['observer_id'])
            elif row['data']['type'] == 'DataPktRx' and row['data']['received'] == 1 and row['observer_id'] == hostNodeId:
                rxPktsRound.append(row['data']['txNode'])
        if debug:
            print('txPktsRound: {}'.format(txPktsRound))
            print('rxPktsRound: {}'.format(rxPktsRound))
        for nodeId in txPktsRound:
            txPkts[nodeId][roundIdx] += 1
        for nodeId in rxPktsRound:
            rxPkts[nodeId][roundIdx] += 1

    # construct dataframe
    reliabilityList = []
    for roundIdx in range(len(tsRounds)-1):
        for nodeId in nodeIds:
            reliabilityList.append({
                'nodeId': nodeId,
                'roundIdx': roundIdx,
                'txPkts': txPkts[nodeId][roundIdx],
                'rxPkts': rxPkts[nodeId][roundIdx],
            })
    dfOut = pd.DataFrame(reliabilityList)
    dfOut['reliability'] = [rx/tx if tx != 0 else np.nan for rx,tx in zip(dfOut.rxPkts, dfOut.txPkts)]

    return dfOut


def processReliability(df, testNo, roundOffset, outputDir):
    nodeIds = sorted(set(df.nodeId))

    # plot reliability for each roundId and node
    fig, ax = plt.subplots(figsize=(7, 3))
    fig.set_tight_layout(True)
    colors = iter(cm.gist_rainbow(np.linspace(0, 1, len(nodeIds))))
    for nodeId, gdf in df.groupby('nodeId'):
        ax.plot(gdf.roundIdx, gdf.reliability, marker='.', color=next(colors), label='Node {}'.format(nodeId))
    # area to cover values not used for aggregation
    trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes) # use data coordinates for the x-axis and the axes coordinates for the y-axis
    ax.fill_between([0, 0, roundOffset, roundOffset], [0, 0, 0, 0], [0, 1, 1, 0], facecolor='grey', alpha=0.15, transform=trans)
    if plotTitle:
        ax.set_title('Reliability over time')
    ax.set_xlabel('Round No.')
    ax.set_ylabel('Reliability [%]')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.savefig(os.path.join(outputDir, '{}_reliability.pdf'.format(testNo)), bbox_inches="tight")

    # # stats per node (aggregated over all rounds)
    df = df[df.roundIdx >= roundOffset]
    for nodeId, gdf in df.groupby('nodeId'):
        if gdf.txPkts.sum() == 0:
            raise Exception('Node with nodeId={} did not send any packets!'.format(nodeId))
        print('Node {}: {}/{} ({:.3f}%)'.format(nodeId, gdf.rxPkts.sum(), gdf.txPkts.sum(), gdf.rxPkts.sum()/gdf.txPkts.sum()*100.))
    # reliability (bar plot)
    relKeys, relVals = zip(*[(nodeId, gdf.rxPkts.sum()/gdf.txPkts.sum()) for nodeId, gdf in df.groupby('nodeId')])
    txKeys, txVals = zip(*[(nodeId, gdf.txPkts.sum()/len(gdf.txPkts)) for nodeId, gdf in df.groupby('nodeId')]) # assumption: target is to have exactly 1 slot per round
    if np.sum(np.diff([len(gdf.txPkts) for nodeId, gdf in df.groupby('nodeId')])) != 0: # ensure that we have the same number of rounds for all nodes
      raise Exception('Number of rounds differs for different nodeIds!')
    fig, ax = plt.subplots(figsize=(6,3))
    width = 0.3
    # ax.bar(range(len(relKeys)), height=np.asarray(relVals)*100.)
    ax.bar(np.arange(len(relKeys))-width/2, height=np.asarray(relVals)*100., width=width, label='#Rx/#Tx')
    ax.bar(np.arange(len(txKeys))+width/2, height=np.asarray(txVals)*100., width=width, label='#Tx/#Slots')
    ax.set_xticks(range(len(relKeys)))
    ax.set_xticklabels(relKeys)
    ax.set_xticks(ax.get_xticks()[::1]) # set tick inverval
    if plotTitle:
        ax.set_title('Reliability')
    ax.set_xlabel('Node ID')
    ax.set_ylabel('Percentage [%]')
    ax.set_ylim(0, 100)
    ax.legend(loc='lower right')
    plt.savefig(os.path.join(outputDir, 'fl{}_reliability.pdf'.format(testNo)), bbox_inches="tight")


def extractEnergyRocketlogger(tsRounds, hostNodeId, nodeIds, testDir, testNo, downsampleFactor=None):
    ''' Extract energy per round (and per node) from rocketlogger measurements contained in flocklab test results
    Args:
      tsRounds:   List of round start timestamps to consider (note last element is used as end of round only, corresponding round is ignored)
      nodeIds:    List of all node IDs (except host)
      hostNodeId: ID of host node
      testNo:     Directory of FlockLab tests
      testDir:    Flocklab Test Number
    '''
    energyList = []

    for nodeId in nodeIds:
        pwrDf = pd.DataFrame()
        powerRldFile = os.path.join(testDir, str(testNo), 'powerprofiling.{}.{}.rld'.format(nodeId, nodeId))
        if not os.path.isfile(powerRldFile):
            raise Exception('RocketLogger file (rld) of node {} not found!'.format(nodeId))
        # sys.stdout = open(os.devnull, 'w') # disable printout (to prevent  rocketlogger printout), sideeffect: terminal sometimes gets stuck in disabled printing state
        rld = RocketLoggerData(powerRldFile)
        rld.merge_channels()
        # sys.stdout = sys.__stdout__ # re-enable printout
        ts = rld.get_time(absolute_time=True, time_reference='network')
        pwrDf['timestamp'] = ts.astype('uint64') / 1e9   # convert to s
        pwrDf['current_A'] = rld.get_data('I1')
        pwrDf['voltage_V'] = rld.get_data('V2') - rld.get_data('V1') # voltage difference
        pwrDf['power_W'] = pwrDf['current_A'] * pwrDf['voltage_V']

        if downsampleFactor is not None:
            print('WARNING: downsampling for extractEnergyRocketlogger is active!')
            pwrDf = pwrDf.iloc[::downsampleFactor]

        energyListTmp = list(pwrDf['power_W'][1:] * np.diff(pwrDf['timestamp']))
        pwrDf['energy'] = [energyListTmp[0]] + energyListTmp

        for roundIdx in range(len(tsRounds)-1):
            pwrDfTmp = pwrDf[(pwrDf.timestamp >= tsRounds[roundIdx]) & (pwrDf.timestamp < tsRounds[roundIdx+1])]

            # # DEBUG
            # fig, ax = plt.subplots()
            # ax.plot(pwrDfTmp['timestamp'], pwrDfTmp['energy']*1e6, marker='.')
            # ax.set_title('Energy Consumption per RL sample {}'.format(nodeId))
            # ax.set_xlabel('Time')
            # ax.set_ylabel('Energy [uJ]')
            # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            # plt.show()

            energyList.append({
                'nodeId': nodeId,
                'roundIdx': roundIdx,
                'energy': np.sum(pwrDfTmp.energy),
            })

    return pd.DataFrame(energyList)


def extractEnergyGpio(tsRounds, hostNodeId, nodeIds, testDir, testNo, gpioFlood=GPIO_FLOOD, gpioTx=GPIO_TX, gpioRx=GPIO_RX, pwrFlood=18.5e-3, pwrTx=175e-3, pwrRx=52e-3):
    ''' Extract energy per round (and per node) from GPIO tracing contained in flocklab test results
    Args:
      tsRounds:   List of round start timestamps to consider (note last element is used as end of round only, corresponding round is ignored)
      nodeIds:    List of all node IDs (except host)
      hostNodeId: ID of host node
      testNo:     Directory of FlockLab tests
      testDir:    Flocklab Test Number
      gpioFlood:  Label of the GPIO pin which indicates whether a Gloria flood is ongoing
      gpioTx:     Label of the GPIO pin which indicates radio Tx
      gpioRx:     Label of the GPIO pin which indicates radio Rx
      pwrFlood    Power consumption of node in flood mode (executing flood without Tx or RX) in Watts
      pwrTx       Power consumption of node in Tx mode in Watts
      pwrRx       Power consumption of node in Rx mode in Watts
    '''
    energyList = []

    # read gpio tracing
    gpioPath = os.path.join(testDir, str(testNo), 'gpiotracing.csv')
    print('Reading GPIO data from {}'.format(gpioPath))
    if not os.path.isfile(gpioPath):
        raise Exception('GPIO tracing file not available!')
    gpioDf = pd.read_csv(gpioPath, float_precision='round_trip')

    gpioPins = [gpioFlood, gpioTx, gpioRx]
    for nodeId in nodeIds:
        dfNode = gpioDf[gpioDf.observer_id == nodeId]
        if len(dfNode) == 0:
            raise Exception('GPIO log is empty for node {}'.format(nodeId))
        for roundIdx in range(len(tsRounds)-1):
            dfNodeRound = dfNode[(dfNode.timestamp >= tsRounds[roundIdx]) & (dfNode.timestamp < tsRounds[roundIdx+1])]
            tAcc = {pin: 0 for pin in gpioPins} # accumulated time
            for pin in gpioPins:
                df = dfNodeRound[dfNodeRound.pin_name == pin]
                # pin state at beginning of round
                pinStateBeginning = getPinStateAtTime(tsRounds[roundIdx], dfNode[dfNode.pin_name == pin])
                lastPinState = pinStateBeginning
                if pinStateBeginning == 1:
                    timeStart = tsRounds[roundIdx]
                    print('WARNING: {} pin of node {} is high over round boundary at t={}s'.format(pin, nodeId, tsRounds[roundIdx]))
                else:
                    timeStart = None
                # pin state changes within round
                for idx, row in df.iterrows():
                    pinState = row['value']
                    if lastPinState == 0 and pinState == 1:
                        timeStart = row['timestamp']
                    elif lastPinState == 1 and pinState == 0:
                        tAcc[pin] += row['timestamp'] - timeStart
                    else:
                        raise Exception('Unexpected sequence of pin values for pin {} on node {} in round starting at t={}s: lastPinState={}, pinState={}. Potentially pin is not 0 over round boundaries.'.format(pin, nodeId, tsRounds[roundIdx], lastPinState, pinState))
                    lastPinState = pinState
                # pin state at end of round
                if lastPinState == 1:
                    tAcc[pin] += tsRounds[roundIdx+1] - timeStart
                    print('WARNING: {} pin of node {} is high over round boundary at t={}s'.format(pin, nodeId, tsRounds[roundIdx+1]))
            # subtract tTx and tRx from tFlood
            tAcc[gpioFlood] -= (tAcc[gpioTx] + tAcc[gpioRx])
            # calculate energy consumption
            energyTmp = tAcc[gpioFlood]*pwrFlood + tAcc[gpioTx]*pwrTx + tAcc[gpioRx]*pwrRx
            energyList.append({
                'nodeId': nodeId,
                'roundIdx': roundIdx,
                'energy': energyTmp,
            })

    return pd.DataFrame(energyList)


def processEnergy(df, testNo, label, roundOffset, outputDir):
    '''
    Args:
        df:      data
        label:   'rl': rocketlogger, 'gpio': GPIO
    '''
    nodeIds = sorted(set(df.nodeId))

    # print aggregated energy per node
    for nodeId, gdf in df.groupby('nodeId'):
        print('Node {}: min={:.3f}/median={:.3f}/max={:.3f} mJ'.format(nodeId, np.min(gdf.energy)*1e3, np.median(gdf.energy)*1e3, np.max(gdf.energy)*1e3))

    # plot energy vs. roundId for each node and round
    fig, ax = plt.subplots(figsize=(7,3))
    fig.set_tight_layout(True)
    colors = iter(cm.gist_rainbow(np.linspace(0, 1, len(nodeIds))))
    for nodeId, gdf in df.groupby('nodeId'):
        ax.plot(gdf.roundIdx, gdf.energy*1e3, marker='.', color=next(colors), label='Node {}'.format(nodeId))
    # area to cover values not used for aggregation
    trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes) # use data coordinates for the x-axis and the axes coordinates for the y-axis
    ax.fill_between([0, 0, roundOffset, roundOffset], [0, 0, 0, 0], [0, 1, 1, 0], facecolor='grey', alpha=0.15, transform=trans)
    if plotTitle:
        ax.set_title('Energy Consumption per Round ({})'.format(label))
    ax.set_xlabel('Round No.')
    ax.set_ylabel('Energy Consumption [mJ]')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.savefig(os.path.join(outputDir, 'fl{}_energyOverTime_{}.pdf'.format(testNo, label)), bbox_inches="tight")

    # plot energy for each node (aggregated for all rounds)
    np.seterr('warn')  # prevent "FloatingPointError: underflow encountered in exp" which occur in certain cases when plotting violin plots
    df = df[df.roundIdx >= roundOffset]
    fig, ax = plt.subplots(figsize=(6,3))
    keys, vals = zip(*[(nodeId, gdf.energy*1e3) for nodeId, gdf in df.groupby('nodeId')])
    # ax.boxplot(vals)
    ax.violinplot(vals)
    ax.set_xticks(np.arange(1, len(keys) + 1)) # only needed for violinplot
    ax.set_xticklabels(keys)
    ax.set_ylim(bottom=0.)
    ax.set_xlabel('Node ID')
    ax.set_ylabel('Energy Consumption per Round [mJ]')
    if plotTitle:
        ax.set_title('Energy ({})'.format(label))
    plt.savefig(os.path.join(outputDir, 'fl{}_energy_{}.pdf'.format(testNo, label)), bbox_inches="tight")
    np.seterr('raise')

def processResults(testNo, testDir, roundOffset=15, outputDir='output'):
    os.makedirs(outputDir, exist_ok=True)
    plt.close('all')

    dfd, tsRounds, hostNodeId, nodeIds = extractSerialData(testNo, testDir)

    dumpDict = {}

    # reliability (printing)
    print('===== Reliability =====')
    reliabilityDf = extractReliability(dfd, tsRounds, hostNodeId, nodeIds)
    dumpDict['reliabilityDf'] = reliabilityDf
    processReliability(reliabilityDf, testNo, roundOffset, outputDir)

    # energy (based on rocketlogger measurements) (printing)
    print('===== Energy (RocketLogger) =====')
    try:
        energyRlDf = extractEnergyRocketlogger(tsRounds, hostNodeId, nodeIds, testDir, testNo, downsampleFactor=8)
        dumpDict['energyRlDf'] = energyRlDf
        processEnergy(energyRlDf, testNo, label='rl', roundOffset=roundOffset, outputDir=outputDir)
    except Exception as e:
        print('WARNING: {}'.format(e))

    # energy (based on GPIO tracing (Tx, Rx, Gloria flood active))
    print('===== Energy (GPIO) =====')
    pwrFlood=18.5e-3
    pwrTx=204e-3 # for +4dBm
    # pwrTx=175e-3 # for +1dBm
    pwrRx=52e-3
    energyGpioDf = extractEnergyGpio(tsRounds, hostNodeId, nodeIds, testDir, testNo, pwrFlood=pwrFlood, pwrTx=pwrTx, pwrRx=pwrRx)
    dumpDict['energyGpioDf'] = energyGpioDf
    processEnergy(energyGpioDf, testNo, label='gpio', roundOffset=roundOffset, outputDir=outputDir)

    # save to pickle file
    with open(os.path.join(outputDir, 'fl{}_data.pkl'.format(testNo)), 'wb') as f:
        pickle.dump(dumpDict, f)

################################################################################
# Main
################################################################################

if __name__ == "__main__":
    # # check arguments
    # if len(sys.argv) < 2:
    #     print("no test number specified!")
    #     sys.exit(1)
    # testNoList = map(int, sys.argv[1:])
    # testDir = os.getcwd()

    # # DEBUG override
    # testNoList = [<test ID>]
    # testDir = '<path>'

    for testNo in testNoList:
        print('%%%%%%%%%%% testNo: {} %%%%%%%%%%%'.format(testNo))
        processResults(testNo, testDir, roundOffset=15, outputDir=outputDir)

    # DEBUG
    with open(os.path.join(outputDir, 'fl{}_data.pkl'.format(testNo)), 'rb') as f:
        p = pickle.load(f)
