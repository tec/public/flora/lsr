#!/usr/bin/env python3

import time
import sys
import base64
import binascii
import socket
import enum
import sys
import argparse
import numpy as np
import os
from datetime import datetime as dt

from sx1262_sim.lsr import LsrNode, LsrStreamReq, LsrScheduleSlots, LsrScheduleParticipUpdate
from sx1262_sim.sim import Sim
from sx1262_sim import sx1262

################################################################################
# config
################################################################################

# NOTE: The following parameters MUST match the ones of used in the C implementation
# interface via serial connection
b64_identifier     = "B64="
max_pkt_len        = 256
pkt_hdr_len        = 10
FLOCKLAB_BASE_PORT = 50100
# LSR
# NOTE: make sure that LSR_MAX_DATA_SLOTS is large enough for any constellation of the execution
LSR_MIN_NODE_ID    = 1
LSR_MAX_NODE_ID    = 32
LSR_NUM_TX         = 2
LSR_NUM_HOPS       = [2, 4]
lsr_slot_length    = [600e-3, 150e-3] # TODO: calculate using other parameters and using sx1262 lib
LSR_MODULATIONS    = [5, 10]
LSR_SCHED_PERIOD   = 10

OUTPUT_DIR = './output/output_schedif'


# derived constants
LSR_MAX_NUM_NODES   = LSR_MAX_NODE_ID - LSR_MIN_NODE_ID + 1
LSR_DELAY_MASK_SIZE = int((LSR_MAX_NUM_NODES + 7) / 8)

################################################################################
# Helper functions
################################################################################
def validDatetime(s):
    try:
        return dt.strptime(s, "%Y-%m-%d_%H:%M:%S")
    except ValueError:
        msg = "not a valid date: {}".format(s)
        raise argparse.ArgumentTypeError(msg)

################################################################################
# LSR Scheduler Interface Packet Class
################################################################################

# Packet types for the interface between the LSR host (flora C code) and the external scheduler (Python)
class LsrSchedIfPktType(enum.IntEnum):
    INV             = 0
    RX_IDX          = 1
    DELAY_MASK      = 2
    PARTICIP_UPDATE = 3
    SLOTS           = 4
    STREAM_REQ      = 5
    STREAM_ACK      = 6

class LsrSchedIfPkt():
    '''Class to parse and represent sched_if packets'''

    def __init__(self):
        self.type    = LsrSchedIfPktType.INV
        self.seqNo   = None
        self.arg     = None
        self.payload = b''


    def load(self, pktRaw):
        '''Load packet from raw bytes, i.e. parse packet'''
        self.type     = LsrSchedIfPktType(pktRaw[4])
        self.seqNo    = int.from_bytes(pktRaw[6:8], 'little')
        self.arg      = int.from_bytes(pktRaw[8:10], 'little')
        self.payload  = pktRaw[pkt_hdr_len:]
        pktPayloadLen = pktRaw[5]
        pktCrc        = int.from_bytes(pktRaw[0:4], 'little')
        calcCrc       = binascii.crc32(pktRaw[4:])
        # self._print("=> Packet of type {} ({}) received ({} bytes)".format(self.type, LsrSchedIfPktType(self.type).name, pktPayloadLen)) # DEBUG
        # check length and crc
        if (pktPayloadLen < 0) or (pktPayloadLen > (max_pkt_len - pkt_hdr_len)) or (pktPayloadLen != len(self.payload)):
            raise Exception(f"Invalid payload length ({pktPayloadLen}, {len(self.payload)})!")
        elif calcCrc != pktCrc:
            raise Exception("Invalid CRC!")


    def dump(self):
        '''Generate raw bytes, i.e. compose packet'''
        # check if required info is available
        if (type(self.type) != LsrSchedIfPktType) or (self.type == LsrSchedIfPktType.INV):
            raise Exception('dump: Invalid packet type!')
        elif self.seqNo is None:
            raise Exception('dump: Invalid seqNo!')
        elif self.arg is None:
            raise Exception('dump: Invalid arg!')
        elif type(self.payload) is not bytes:
            raise Exception('dump: Invalid payload type ({})!'.format(type(self.payload)))

        # compose header
        data        = bytearray(pkt_hdr_len)
        data[4]     = self.type.value
        data[5]     = len(self.payload)
        data[6:8]   = self.seqNo.to_bytes(2, 'little')
        data[8:10]  = self.arg.to_bytes(2, 'little')
        # append payload
        data       += self.payload
        # calculate crc
        data[0:4]   = binascii.crc32(data[4:]).to_bytes(4, 'little')

        return bytes(data)


    def __repr__(self):
        return 'LsrSchedIfPkt(type={}, seqNo={}, arg={}, payloadLen={})'.format(LsrSchedIfPktType(self.type).name, self.seqNo, self.arg, len(self.payload))


class LsrStreamReqSchedif():
    '''Class to parse and represent stream requests'''

    def __init__(self, rawData):
        if rawData == 3:
            raise Exception('Invalid STREAM_REQ pkt size!')
        ba = bytearray(rawData)
        reset_ipi = int.from_bytes(ba[0:2], 'little')
        self.reset  = 0x8080 & reset_ipi
        self.ipi    = 0x7fff & reset_ipi
        self.nodeId = ba[2] + LSR_MIN_NODE_ID


    def __repr__(self):
        return 'LsrStreamReqSchedif(reset={}, ipi={}, nodeId={})'.format(self.reset, self.ipi, self.nodeId)


################################################################################
# LSR Scheduler Interface Class
################################################################################

class LsrSchedIf(object):
    """ Class to enable interactions between LSR (running on real hardware in the FlockLab 2 testbed) and the external LSR scheduler (Python, part of the simulation) """

    def __init__(self, hostname, socketport, lsrObj, logFilename=None, outputDir='./output'):
        self.hostname = hostname
        self.socketport = socketport
        self.lsr = lsrObj
        self.outputDir = outputDir
        os.makedirs(self.outputDir, exist_ok=True)
        self._logFile = open(os.path.join(outputDir, logFilename), 'w')

        self.seqNoTx = 0
        self.seqNoRx = 0

        # data buffer for reading from socket (keep state globally to object to be able to piece together broken up lines)
        self.dataBuf = ''

        # state for state machine (receive RX_IDX and STREAM_REQ, process, send results)
        self.rxIdxPktRcvd = False
        self.streamReqPktRcvd = False
        self.currModIdx = None

        # variables for data collection
        self.hdm = None
        self.sr  = None


    def parseData(self, data):
        """ Process a chunk of serial data, potentially parse completely received packets """

        # ensure correct processing even if data ends in the middle of a line
        self.dataBuf += data
        dataSplit = self.dataBuf.split('\n')
        lineList = dataSplit[:-1]
        self.dataBuf = dataSplit[-1]

        for line in lineList:
            if len(line) == 0:
                continue

            # self._print(line.strip()) # DEBUG: print entire serial log

            if b64_identifier in line:
                content = line.split(b64_identifier)[1]
                pktBytes = base64.b64decode(content)

                pkt = LsrSchedIfPkt()
                pkt.load(pktBytes)
                if self.seqNoRx != pkt.seqNo:
                    raise Exception('Sequence number of received packet does not match (seqNoRx={}, pkt.seqNo={})'.format(self.seqNoRx, pkt.seqNo))
                self.seqNoRx += 1
                self._print(f'RECEIVED: {pkt}')

                if pkt.type == LsrSchedIfPktType.RX_IDX:
                    # update state machine
                    if (self.currModIdx is not None) or (self.streamReqPktRcvd):
                        raise Exception('Wrong sequence of packets received!')
                    self.rxIdxPktRcvd = True
                    self.currModIdx = pkt.arg
                    # extract hop distance measurements
                    if len(pkt.payload) != LSR_MAX_NUM_NODES:
                        raise Exception('Wrong number of measurements in RX_IDX pkt!')
                    rx_idx = [e if e < 255 else None for e in bytearray(pkt.payload) ] # 255 means invalid measurement
                    self.hdm = dict()
                    for i, rxIdx in enumerate(rx_idx):
                        if rxIdx is None:
                            continue
                        nodeId = i + LSR_MIN_NODE_ID
                        self.hdm[nodeId] = rxIdx + 1
                    self._print(f'hopDistanceMeasurements: {self.hdm}')
                elif pkt.type == LsrSchedIfPktType.STREAM_REQ:
                    # upate state machine
                    if (self.currModIdx != pkt.arg) or (not self.rxIdxPktRcvd):
                        raise Exception('Wrong sequence of packets received!')
                    self.streamReqPktRcvd = True
                    # extract stream req
                    if len(pkt.payload) not in (0, 3):
                        raise Exception('Invalid STREAM_REQ pkt received!')
                    if len(pkt.payload) == 3:
                        self.sr = LsrStreamReqSchedif(pkt.payload)
                        self._print('streamReq: {}'.format(self.sr))
                else:
                    raise Exception('Received unknown packet type: {}'.format(pkt.type))

                if self.rxIdxPktRcvd and self.streamReqPktRcvd:
                    self.updateLsrState()
                    self.sendUpdates()
                    self.lsr._resetRound()
                    # advance time (required for timeout of LSR streams)
                    if self.currModIdx == (len(LSR_MODULATIONS) - 1):
                        self.lsr.node._sim.t += LSR_SCHED_PERIOD
                        self._print('[{}] ---------------'.format(self.lsr.node._sim.t)) # boundary in debug output
                        self.lsr.roundNo += 1
                    # reset state for state machine
                    self.rxIdxPktRcvd = False
                    self.streamReqPktRcvd = False
                    self.currModIdx = None
                    # reset state for data collection
                    self.hdm = None
                    self.sr  = None

    def updateLsrState(self):
        """ Update state of LSR object by inserting received info into LSR object """
        # hop distance measurements
        for nodeId, hd in self.hdm.items():
            if nodeId in self.lsr.hopDistanceMeasurementsHost[self.currModIdx]:
                self.lsr.hopDistanceMeasurementsHost[self.currModIdx][nodeId].append(hd)
                self.lsr.tsLastDataReceived[nodeId] = self.lsr.node.getTs() # update timestamp to prevent deregistration of node
            else:
                self._print(f'WARNING: Could not insert hop distance measurement from nodeId={nodeId} into LSR object (rcIdx={self.currModIdx})')
        # stream reqs
        if self.sr is not None:
            lsrStreamReq = LsrStreamReq(msg={
                'nodeId':               self.sr.nodeId,
                'changeExistingStream': 0, # currently changing data rate of an existing stream is not used
                'msgSize':              30,
                'nMsgs':                1,
                'period':               LSR_SCHED_PERIOD,
            })
            self.lsr.streamRequestQueue[self.currModIdx].append(lsrStreamReq)

        # update state
        self.lsr.updateRegisteredNodes(singleRcIdx=self.currModIdx)
        self.lsr.updateThinning(singleRcIdx=self.currModIdx)
        self.lsr.updateDelayMask(singleRcIdx=self.currModIdx)

        self._print('LSR: registeredNodes (modIdx={}): {}'.format(self.currModIdx, self.lsr.registeredNodes[self.currModIdx]))

        # prepare all data without generating simulation events
        self.lsr.currRcIdx = self.currModIdx # needs to be set as it is used by sendScheduleFlood()
        self.lsr.sendScheduleFlood(woEventGen=True)


    def sendUpdates(self):
        """ Use state to compose and send updates """
        pkt = LsrSchedIfPkt()
        pkt.arg = self.currModIdx
        lastSchedule = self.lsr.lastSchedule[self.currModIdx]

        # send DELAY_MASK
        pkt.type = LsrSchedIfPktType.DELAY_MASK
        pkt.payload = lastSchedule['delayMask'].to_bytes(4, 'little')
        # DEBUG
        # pkt.payload = 0x00000000.to_bytes(4, 'little')
        # pkt.payload = 0xffffffff.to_bytes(4, 'little')
        self.socketWritePkt(pkt)
        self._print('delayMask: {:032b}'.format(lastSchedule['delayMask']))

        # send PARTICIP_UPDATE
        nodeIdSize = LsrScheduleSlots(msg={'slots': [0]}).size
        participUpdateMsgBytes = lastSchedule['raw'][lastSchedule['nDataSlots']*nodeIdSize:]
        participUpdateSize = LsrScheduleParticipUpdate(msg={'nodeIdTx': 0, 'participMask': 0}).size
        if len(participUpdateMsgBytes) % participUpdateSize != 0:
            raise Exception('Size of particip updates is not an integer multiple of the size of a particip update!')
        if len(participUpdateMsgBytes):
            # invert bits of 'particip mask' to convert to 'disable mask' (bit=1 means node does not participate)
            participUpdateMsgBytesInv = b''
            tmpPrintList = []
            for i in range(int(len(participUpdateMsgBytes)/participUpdateSize)):
                pu = LsrScheduleParticipUpdate(raw=participUpdateMsgBytes[i*participUpdateSize:(i+1)*participUpdateSize])
                tmpPrintList.append( (pu['nodeIdTx'], pu['participMask']) )
                pu['participMask'] = ~np.uint32(pu['participMask'])
                participUpdateMsgBytesInv += pu.raw
            pkt.type = LsrSchedIfPktType.PARTICIP_UPDATE
            pkt.payload = participUpdateMsgBytesInv
            self.socketWritePkt(pkt)
            for nodeIdTx, participMask in tmpPrintList:
                self._print('participUpdate (nodeIdTx={}): {:032b}'.format(nodeIdTx, participMask))

        # send SLOTS
        pkt.type = LsrSchedIfPktType.SLOTS
        scheduleSlotsByteList = lastSchedule['raw'][:lastSchedule['nDataSlots']*nodeIdSize]
        pkt.payload = b''.join(scheduleSlotsByteList)
        # # DEBUG
        # regNodeIds = preRegNodeIds[self.currModIdx]
        # regNodeIdsBytes = [nodeId.to_bytes(2, 'little') for nodeId in regNodeIds]
        # regNodeIdsRaw = b''.join(regNodeIdsBytes)
        # pkt.payload = regNodeIdsRaw
        self.socketWritePkt(pkt)
        self._print('scheduleSlots: {}'.format([int.from_bytes(b''.join(scheduleSlotsByteList[i*nodeIdSize:(i+1)*nodeIdSize]), 'little')  for i in range(int(len(scheduleSlotsByteList)/nodeIdSize))]))

        # send STREAM_ACK
        if lastSchedule['streamAck'] != 0: # streamAck==0 in schedule means no stream to ack
            pkt.type = LsrSchedIfPktType.STREAM_ACK
            streamAckNodeIdx = lastSchedule['streamAck']
            pkt.payload = streamAckNodeIdx.to_bytes(nodeIdSize, 'little')
            self.socketWritePkt(pkt)
            self._print('streamAck: nodeId={}'.format(lastSchedule['streamAck']))


    def socketRead(self):
        """ Read chunk from serial data (via TCP due to flocklab interface) """
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.hostname, self.socketport))
        self._print('[{}] ---------------'.format(self.lsr.node._sim.t)) # boundary in debug output
        try:
            while True:
                # blocking receive
                data = self.s.recv(1024)
                if data:
                    self.parseData(data.decode())
                else: # if no data received -> connection no longer available
                    self._print('No more data to receive. Terminating...')
                    break
        except:
            self.s.close()
            raise


    def socketWritePkt(self, pkt):
        """ Write packet to serial """
        # add sequence number
        pkt.seqNo = self.seqNoTx
        self.seqNoTx += 1

        self._print(f'SENT: {pkt}')

        # send packet by writing one line
        pktBytes = pkt.dump()
        pktBase64 = base64.b64encode(pktBytes)
        pktLine = pktBase64 + b'\n'
        self.s.sendall(pktLine)


    def start(self, startTime=None):
        """ Start interactions via serial communication """
        self.seqNoTx = 0

        if type(startTime) == dt:
            diff = startTime - dt.now()
            self._print('Waiting {}s until startTime {}'.format(round(diff.total_seconds()), startTime))
            while dt.now() < startTime:
                time.sleep(1.0)


        # continuously try to connect
        while True:
            try:
                self.socketRead()
            except ConnectionRefusedError:
                self._print(F"Cannot connect to host {self.hostname} on port {self.socketport}")
            except ConnectionResetError as err:
                self._print(f"{err}")
                self._logFile.close()
                sys.exit()
            except KeyboardInterrupt:
                self._print("aborted")
                self._logFile.close()
                sys.exit()

            time.sleep(1.0)

    def _print(self, outText):
        print(outText, file=self._logFile)
        print(outText)


################################################################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--testno', help='Testbed mode (interact with nodes of the FlockLab 2 testbed)', metavar='<testNo>', default=None, type=int)
    parser.add_argument('-d', '--debug', help='Debug mode (interact with emulation)', action='store_true', default=False)
    parser.add_argument('-i', '--hostid', help='ID of the host node', metavar='<nodeId>', default=None, type=int)
    parser.add_argument('-s', '--starttime', help='Time at which the schedif should start trying to connect (time zone of system)', metavar='<datetime yyyy-mm-dd_hh:mm:ss>', default=None, type=validDatetime)
    parser.add_argument('-c', '--scenario', help='Choose pre-defined scenario (defines host ID & sets of pre-registered nodes)', metavar='<idx of scenario>', default=None, type=int)
    args = parser.parse_args()

    ## pre-defined scenarios
    # default (bootstrap, no pre-registering)
    preRegNodeIds = dict([
        (0, []), # Mod0
        (1, []), # Mod1
    ])
    if args.scenario is not None:
        if (args.scenario == 1):
            # fl2_s1
            args.hostid = 7
            preRegNodeIds = dict([
                (0, [15, 17, 25, 30]), # Mod0
                (1, [1, 4, 6, 7, 12, 13, 24, 26, 27]), # Mod1
            ])
        elif (args.scenario == 2):
            # fl2_s2
            args.hostid = 1
            preRegNodeIds = dict([
                (0, [15, 17, 25, 30]), # Mod0
                (1, [4, 6, 7, 12, 13, 24, 26, 27]), # Mod1
            ])
        elif (args.scenario == 3):
            # fl2_s3
            args.hostid = 15
            preRegNodeIds = dict([
                (0, [1, 4, 6, 7, 12, 13, 15, 17, 24, 25, 26, 27, 30]), # Mod0
                (1, []), # Mod1
            ])
        elif (args.scenario == 4):
            # fl2_s4
            args.hostid = 24
            preRegNodeIds = dict([
                (0, [15, 17, 25, 30]), # Mod0
                (1, [1, 4, 6, 7, 12, 13, 26, 27]), # Mod1
            ])
        else:
            raise Exception(f'Unknown scenario \'{args.scenario}\'')


    # sanity checks
    if (args.testno is not None) and args.debug:
        raise Exception('Testbed and debug modes cannot be used simultaneously!')
    if (not args.debug) and (args.testno is None):
        raise Exception('A valid ID of the test must be provided when using the testbed mode! (provided testno={})'.format(args.testno))
    if (args.hostid is None):
        raise Exception('A valid nodeId of the host must be provided when using the testbed mode! (provided hostid={})'.format(args.hostid))


    # set hostname, socketport and outputLabel
    if args.debug:
        hostname = "127.0.0.1" # debugging with local server replaying serial log (lsr_schedif_emulate.py)
        outputLabel = 'debug'
        socketport = FLOCKLAB_BASE_PORT
    else:
        hostname = "flocklab.ethz.ch"
        outputLabel = str(args.testno)
        socketport = FLOCKLAB_BASE_PORT + args.hostid  # port number is 50100 + observer ID

    # create LSR object
    radioConfigs = []
    for modIdx in LSR_MODULATIONS:
        rc = sx1262.getFloraConfig(modIdx=modIdx)
        # additional fields required for the simulation
        rc.frequency = 868100000  # in Hz
        rc.txPower   = 0
        rc.sensitivitySrcDatasheet = False
        radioConfigs.append(rc)
    # sim is required for access to time information
    sim = Sim(
        logFileName = 'simLog_{}.txt'.format(outputLabel),
        outputDir   = OUTPUT_DIR,
    )
    sim.setRadioConfigs(radioConfigs)
    sim.timeDrift       = False
    lsrNode = LsrNode(
        radioConfigs = radioConfigs,
        hostNodeId   = args.hostid,
        nodeId       = args.hostid,
        nTx          = LSR_NUM_TX,
        numHops      = LSR_NUM_HOPS,
        slotLength   = lsr_slot_length,
        period       = LSR_SCHED_PERIOD,
        streamPeriod = LSR_SCHED_PERIOD,
        minNodeId    = LSR_MIN_NODE_ID,
        maxNodeId    = LSR_MAX_NODE_ID,
        statsDepth   = 3,
        thinningHopDistance       = True,
        thinningConnectivityGraph = True,
        deregisteringThreshold    = None, # if None, default=3*period  is used
        bpsRoundThreshold         = 100,  # threshold for triggering update of set of best paths (in number of rounds)
    )
    lsrNode.setSim(sim)
    lsr = lsrNode.lsr

    # initialize LSR state
    for modIdx, nodeIdList in preRegNodeIds.items():
        for nodeId in nodeIdList:
            lsr.preRegisterNode(nodeId=nodeId, rcIdx=modIdx)
    lsr.start(isHost=True, woEventGen=True)

    # create LSR schedule interface object
    lsi = LsrSchedIf(
        hostname    = hostname,
        socketport  = socketport,
        lsrObj      = lsr,
        logFilename = 'schedifLog_{}.txt'.format(outputLabel),
        outputDir   = OUTPUT_DIR
    )

    lsi._print('selected scenario: {}'.format(args.scenario))
    lsi._print('using host node: {}'.format(args.hostid))
    lsi._print('preRegisterNodes: {}'.format(lsr.preRegisterNodes))
    lsi._print('Registered nodes: {}'.format(lsr.registeredNodes))

    # start interactions with LSR nodes
    lsi.start(args.starttime)

    ############################################################################

    # # Test LsrSchedIfPkt class
    # pktOrigB64 = 'NkS/0gEgCQAAAP//////////////////////////////////////////'
    # pktBytesOrig = base64.b64decode(pktOrigB64)
    # pkt = LsrSchedIfPkt()
    # pkt.load(pktBytesOrig)
    # pktBytesDumped = pkt.dump()
    # pktDumpedB64 = base64.b64encode(pktBytesDumped).decode()
    # print(pkt)
    # print(f'pktOrigB64    : {pktOrigB64}')
    # print(f'pktBytesOrig  : {pktBytesOrig} ({len(pktBytesOrig)} bytes)')
    # print(f'pktBytesDumped: {pktBytesDumped} ({len(pktBytesDumped)} bytes)')
    # print(f'pktDumpedB64  : {pktDumpedB64}')


    # # Use class to modify pkts
    # pktOrigB64 = 'ubT36gUDAQAAAAoAHQ=='
    # pktBytesOrig = base64.b64decode(pktOrigB64)
    # pkt = LsrSchedIfPkt()
    # pkt.load(pktBytesOrig)
    # print(pkt)
    # pkt.payload = b''
    # print(pkt)
    # print(base64.b64encode(pkt.dump()).decode())
