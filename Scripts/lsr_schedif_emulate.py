#!/usr/bin/env python3

import time
import socket
import pandas
import sys
import argparse
import pathlib

from flocklab import Flocklab

fl = Flocklab()


# config
socket_baseport = 50100
hostname        = "127.0.0.1" # debugging with local server


################################################################################

def send_serial_data(serialPath, hostId):
    # read data
    print('Reading data...')
    serialDf = fl.serial2Df(serialPath, error='ignore')
    serialDf = serialDf[(serialDf.observer_id==hostId) & (serialDf.direction=='r')]

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        socketport = socket_baseport
        s.bind((hostname, socketport))
        # s.setblocking(False)
        # s.settimeout(0)
        print('Listening for connections...')
        s.listen()
        conn, addr = s.accept()
        with conn:
            print(f"Connection from to {addr}")
            for idx, row in serialDf.iterrows():
                # print("Read and discard buffer...")
                # conn.recv(1024) # read and discard data to make sure to empty buffer; does not work, as this is blocking (probably first use non-blocking peeking or separate threat)
                # print("Sending 1 line...")
                line = row['output'] + '\n'
                conn.sendall(line.encode('utf-8'))
                time.sleep(0.001)
            print('Finished sending!')



################################################################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--hostid', help='ID of the host node', metavar='<nodeId>', default=None, type=int)
    parser.add_argument('seriallog', help='Path to the serial log file', metavar='<path to serial log>', type=str)
    args = parser.parse_args()

    print(f'Replaying: {args.seriallog}')
    send_serial_data(args.seriallog, args.hostid)
