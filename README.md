# Long-Short-Range Protocol

## Overview
There are two implementations:
* Hardware implementation for the DPP2 ComBoard (SX1262)
  * **C code** (this STM32CubeIDE project): implements basic LSR scheme and LSR with hop distance thinning (LSR with full thinning is not implemented at the moment, hower the external scheduler can be used for this scheme)
  * **External scheduler** (Python-based, `/Scripts/lsr_schedif.py`): to be used together with C code; implements LSR with full thinning (also supports LSR without thinning and LSR with hop distance thinning)
* Implementation as simulation
  * **Simulation** that implements LSR w/o thinning, LSR with hop distance thinning, and LSR with full thinning
  * Based on Python-based simulation
  * `/Scripts/sx1262_sim/app_lsr.py`

The below execution flow shows how to run the same scenario both on real hardware and in simulation and to run the evaluation to obtain visualizations to compare the results (as integrated into the paper):

<img src="Figures/execution_flow_sim_vs_testbed.png" alt="drawing" style="width:100%;"/>


## How to checkout the repos
*  clone the repository:
  `git clone --recurse-submodule git@gitlab.ethz.ch/tec/public/flora/lsr.git`

## How to compile the C source code
* open the project with STM32CubeIDE
* open the `comboard_lsr.ioc` file and generate the source code (this will create a `Drivers` and `Middlewares` folder)
* adapt configuration in `Inc/app_config.h`
* Compile the project

For more information, consult the [Flora wiki](https://gitlab.ethz.ch/tec/public/flora/wiki#clone-compile-run).

## How to run the C code on the FlockLab 2 testbed
* compile the C code as described above
* optional: run the external scheduler (see below)
* run `python run_flocklab_test.py`


## External Scheduler Interface (ext sched_if)

### Overview

<img src="Figures/lsr_ext_scheduler.png" alt="drawing" style="width:100%;"/>


### Scripts
* `lsr_schedif.py`: interface for exchanging information between the C implementation of LSR running on FlockLab 2 nodes and the Python implementation of LSR acting as external scheduler.
* `lsr_ext_sched_if.py`: original template code, for simple debugging
* `lsr_schedif_emulate.py`: script that allows to replay a serial log from FlockLab 2 test results via a TCP socket; useful for debugging `lsr_schedif.py`


### Running a Test
1. run `python run_flocklab_test.py` (configure within script)
2. run `python lsr_schedif.py -i <hostNodeId> -t <testNo>`

Output:
* LSR running on FlockLab2 nodes (C code): serial log as part of FlockLab2 test results
* schedif: `schedifLog_xxx.txt` in output directory defined in lsr_schedif.py (and console output of `lsr_schedif.py`)
* LSR scheduler (Python code): `simLog_schedif.txt` in output directory


### Replaying a Test (for debugging)
1. run `python lsr_schedif_emulate.py -i <hostNodeId> <serial_log_file>`
2. run `python lsr_schedif.py -i <hostNodeId> -d`

Output:
* schedif: `schedifLog_xxx.txt` in output directory defined in lsr_schedif.py (and console output of `lsr_schedif.py`)
* LSR scheduler (Python code): `simLog_schedif.txt` in output directory


### Payload Structures
`arg` field of packet header represents modulation/rcIdx

#### C -> Python
lsr_sched_if_send_pkt()

* LSR_SCHED_IF_PKT_RX_IDX (always transferred)
  * list of uint8_t
  * size: LSR_MAX_NUM_NODES
* LSR_SCHED_IF_PKT_STREAM_REQ (always transferred)
  * single lsr_stream_req_t element
    * reset_mask (1 bit): reset disable mask request bit
    * ipi_s (15 bit): inter-packet interval in seconds (15-bit)
    * node_idx (8 bits): node ID relative to LSR_MIN_NODE_ID

#### Python -> C
lsr_sched_if_parse_pkt()

* LSR_SCHED_IF_PKT_DELAY_MASK (always transferred)
  * bit field of size LSR_DELAY_MASK_SIZE (4 bytes)
  * bit position corresponds to nodeId (with offset of LSR_MIN_NODE_ID)
* LSR_SCHED_IF_PKT_PARTICIP_UPDATE (transferred only when required)
  * list of lsr_sched_particip_update_t elements
    * 2 bytes nodeId (no offset)
    * 4 bytes (LSR_DELAY_MASK_SIZE) bit field
* LSR_SCHED_IF_PKT_SLOTS (always transferred)
  * list of 2 bytes nodeId
* LSR_SCHED_IF_PKT_STREAM_ACK (transferred only when required)
  * single 2 bytes nodeId
