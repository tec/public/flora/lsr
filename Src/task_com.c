/*
 * task_com.c
 *
 * communication task, runs the LSR protocol and controls the execution of the pre and post tasks
 */

#include "main.h"


/* Private variables ---------------------------------------------------------*/

extern TaskHandle_t  xTaskHandle_pre;
extern TaskHandle_t  xTaskHandle_post;
extern QueueHandle_t xQueueHandle_rx;
extern QueueHandle_t xQueueHandle_tx;
extern QueueHandle_t xQueueHandle_retx;

/* global variables for binary patching the config */
volatile uint16_t         host_id                           = HOST_ID;
static volatile int8_t    gloria_power                      = GLORIA_INTERFACE_POWER;
static volatile uint8_t   gloria_band                       = GLORIA_INTERFACE_RF_BAND;
static volatile uint8_t   lsr_n_tx[LSR_NUM_MODULATIONS]     = { LSR_NUM_TX };
static volatile uint8_t   lsr_num_hops[LSR_NUM_MODULATIONS] = { LSR_NUM_HOPS };
static volatile uint32_t  lsr_period                        = LSR_SCHED_PERIOD;

extern uint32_t data_period;


void listen_timeout(void)
{
  /* nothing to do */
}


/* communication task */
void vTask_com(void const * argument)
{
  LOG_VERBOSE("com task started");

  /* make sure the radio is awake */
  radio_wakeup();

  /* set gloria config values */
  gloria_set_tx_power(gloria_power);
  gloria_set_band(gloria_band);

  /* set LSR config values */
  /*if (lsr_sched_set_period(lsr_period)) {
    LOG_INFO("LSR successfully set period to %lus", lsr_period);
  } else {
    LOG_WARNING("LSR rejects setting period to %lus", lsr_period);
  }
  if (lsr_set_n_tx(lsr_n_tx)) {
    LOG_INFO("LSR successfully set n_tx to %u", lsr_n_tx);
  } else {
    LOG_WARNING("LSR rejects setting n_tx to %u", lsr_n_tx);
  }
  if (lsr_set_num_hops(lsr_num_hops)) {
    LOG_INFO("LSR successfully set num_hops to %u", lsr_num_hops);
  } else {
    LOG_WARNING("LSR rejects setting num_hops to %u", lsr_num_hops);
  }*/

  /* init LSR */
  if (!lsr_init(xTaskGetCurrentTaskHandle(), xTaskHandle_pre, xTaskHandle_post, xQueueHandle_rx, xQueueHandle_tx, xQueueHandle_retx, listen_timeout, IS_HOST)) {
    FATAL_ERROR("LSR init failed");
  }

  /* pre-register the source nodes (must be after lsr_init) */
  const uint16_t src_nodes_mod0[] = { SOURCE_NODES_MOD0 };
  const uint16_t src_nodes_mod1[] = { SOURCE_NODES_MOD1 };
  bool  node_registered = false;
  for (uint32_t i = 0; i < sizeof(src_nodes_mod0) / sizeof(uint16_t); i++) {
    if (IS_HOST) {
#if !LSR_USE_EXT_SCHED /* pre-registering nodes on host does not work when using external scheduler as buffer for stream requests has only one element */
      if (!lsr_sched_register_node(src_nodes_mod0[i], DATA_GENERATION_PERIOD, 0)) {
        LOG_ERROR("failed to register node %u", src_nodes_mod0[i]);
      }
#endif
    } else if (src_nodes_mod0[i] == LSR_NODE_ID) {
      lsr_init_params(0, DATA_GENERATION_PERIOD);
      node_registered = true;
    }
  }
  if (LSR_NUM_MODULATIONS > 1) {
    for (uint32_t i = 0; i < sizeof(src_nodes_mod1) / sizeof(uint16_t); i++) {
      if (IS_HOST) {
#if !LSR_USE_EXT_SCHED /* pre-registering nodes on host does not work when using external scheduler as buffer for stream requests has only one element */
        if (!lsr_sched_register_node(src_nodes_mod1[i], DATA_GENERATION_PERIOD, 1)) {
          LOG_ERROR("failed to register node %u", src_nodes_mod1[i]);
        }
#endif
      } else if (src_nodes_mod1[i] == LSR_NODE_ID) {
        lsr_init_params(1, DATA_GENERATION_PERIOD);
        node_registered = true;
      }
    }
  }
  if (!IS_HOST && !node_registered) {
    lsr_set_ipi(DATA_GENERATION_PERIOD);
  }

  while (lptimer_now() < (IS_HOST ? (LPTIMER_SECOND + LPTIMER_SECOND / 10) : LPTIMER_SECOND));
  lsr_start();

  FATAL_ERROR("LSR task terminated");
}
