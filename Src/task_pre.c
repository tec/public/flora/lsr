/*
 * task_pre.c
 *
 * generates data packets
 */

#include "main.h"


extern QueueHandle_t xQueueHandle_tx;

uint32_t data_period = DATA_GENERATION_PERIOD;


/* Private define ------------------------------------------------------------*/

#ifndef PRE_TASK_RESUMED
#define PRE_TASK_RESUMED()
#define PRE_TASK_SUSPENDED()
#endif /* PRE_TASK_RESUMED */


/* Private variables ---------------------------------------------------------*/


/* Functions -----------------------------------------------------------------*/

void generate_data_pkt(void)
{
  static dpp_message_t msg_buffer;

  /* generate a dummy packet (header only, no data) */
  if (ps_compose_msg(DPP_DEVICE_ID_SINK, DPP_MSG_TYPE_INVALID, 0, 0, &msg_buffer) &&
      xQueueSend(xQueueHandle_tx, &msg_buffer, 0)) {
    LOG_INFO("data packet generated");
#if LSR_LOG_JSON
    LOG_INFO("{\"type\":\"DataPktGen\",\"addedToQueue\":1}");
#endif /* LSR_LOG_JSON */
  } else {
    LOG_WARNING("failed to insert message into transmit queue");
#if LSR_LOG_JSON
    LOG_INFO("{\"type\":\"DataPktGen\",\"addedToQueue\":0}");
#endif /* LSR_LOG_JSON */
  }
}


void vTask_pre(void const * argument)
{
  uint32_t last_pkt = 0;

  LOG_VERBOSE("pre task started");

  if (!IS_HOST) {
    generate_data_pkt();
  }

  /* Infinite loop */
  for(;;)
  {
    PRE_TASK_SUSPENDED();
    xTaskNotifyWait(0, ULONG_MAX, NULL, portMAX_DELAY);
    PRE_TASK_RESUMED();

    /* indicate start of new round with json */
#if LSR_LOG_JSON
    if (IS_HOST) {
      LOG_INFO("{\"type\": \"PreTask\"}");
    }
#endif /* LSR_LOG_JSON */

    uint32_t curr_time_s = (lsr_get_time(0) / 1000000);

    /* generate a node info message if necessary (must be here) */
    if (data_period && !IS_HOST) {
      /* only send other messages once the node info msg has been sent! */
      uint32_t div = (curr_time_s / data_period);
      if (div != last_pkt) {
        /* generate a dummy packet (header only, no data) */
        generate_data_pkt();
        last_pkt = div;
      }
    }
  }
}
